// Drugwash, 2022-2023 ; GPL v2+
// original version by jschug
const Applet = imports.ui.applet;
const AppletManager = imports.ui.appletManager;
const Cinnamon = imports.gi.Cinnamon;
const Clutter = imports.gi.Clutter;
const Cvc = imports.gi.Cvc;
const Dialog = imports.ui.modalDialog;
const Ext = imports.ui.extension;
const Gettext = imports.gettext;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const PopupMenu = imports.ui.popupMenu;
const Settings = imports.ui.settings;
const Signals = imports.signals;
const St = imports.gi.St;
const Util = imports.misc.util;

var DBG = false; // set to false for normal operation
const ITYPE = St.IconType.SYMBOLIC;
const UUID = "pa-equalizer2@drugwash";

const APLTDIR = GetApltDir();
const HLPTXT = APLTDIR + "/README.md";

const CONFIG_DIR_OLD = GLib.get_home_dir() + "/.pulse";
const CONFIG_DIR_NEW = GLib.get_user_config_dir() + "/pulse";
const CONFIG_DIR = GLib.file_test(CONFIG_DIR_NEW, GLib.FileTest.IS_DIR) ? CONFIG_DIR_NEW: CONFIG_DIR_OLD;

const EQCONFIG = CONFIG_DIR + "/equalizerrc";
const EQPRESETS = EQCONFIG + ".availablepresets";
const PRESETDIR1 = CONFIG_DIR + "/presets/";

const EQON = "equalizer";
const EQOFF = "equalizer_off";

var EXEC, LOCAL, EXEGUI, PRESETDIR2, PRESETDIR3, DEFEQ, DEFEQL;
if (DBG) {
	let msg = "\n[" + UUID + "] debug report (part 1):" +
	"\nAPLTDIR=" + APLTDIR + "\nCONFIG_DIR_OLD=" + CONFIG_DIR_OLD +
	"\nCONFIG_DIR_NEW=" + CONFIG_DIR_NEW + "\nCONFIG_DIR=" + CONFIG_DIR +
	"\nPRESETDIR1=" + PRESETDIR1 + "\n[" + UUID + "] end report\n";
	global.log(msg);
}

Gettext.bindtextdomain(UUID, GLib.get_home_dir() + "/.local/share/locale");
const _ = function(strg) {
	let trans = Gettext.gettext(strg);
	if (trans !== strg) return trans;
	return Gettext.dgettext(UUID, strg);
}

/* TRANSLATABLE STRINGS */
const OK		= _("OK");
const CANCEL	= _("Cancel");
const EQNONE	= _("There is NO equalizer installed, OR its location/name are nonstandard.\nThis applet cannot function in such circumstances.\n\nIt will now be disabled.");
const EQONE		= _("There is only one equalizer installed.\nApplet will use it by default.\n\nThere is nothing to choose here.");
const EQCHOICE	= _("Applet found more than one Equalizer installed.\nWhich Equalizer would you want to use with this applet?\n");
const EQDISAB	= _("Equalizer <b>disabled</b>");
const EQENAB	= _("Equalizer <b>enabled</b>");
const MSGPRST	= _("Preset:");
const MSGNONE	= _("<none>"); // singular, gender according to MSGPRST above, if applicable
const MSGOUT	= _("Output:");
/* primary menu items */
const PM_ENAB	= _("EQ enabled");
const PM_PRES	= _("Presets");
const PM_SETT	= _("Settings");
/* context-menu items */
const CM_NOTF	= _("Show notifications");
const CM_STRT	= _("Enable at startup");
const CM_SLCT	= _("Select EQ...");
const CM_HELP	= _("Help");
const CM_RELD	= _("Reload applet [forced]");

function GetApltDir() {
	var type = Ext.Type["APPLET"]
	let dir = Ext.findExtensionDirectory(UUID, type.userDir, type.folder);
	return dir.get_path();
}

function HasEq() {
	let bins = ["/usr/bin/pulseaudio-equalizer-ladspa", "/usr/local/bin/pulseaudio-equalizer-ladspa"
		, "/usr/bin/pulseaudio-equalizer", "/usr/local/bin/pulseaudio-equalizer"];
	let inst = [];
	for (let i=0; i<bins.length; ++i) {
		if (GLib.file_test(bins[i], GLib.FileTest.EXISTS)) inst.push(bins[i]);
	}
	if (DBG) global.log("EQ found " + inst.length + ": " + inst.toString());
// we need to check 'which' one is the default, but let user choose
	try {
		DEFEQ = GLib.find_program_in_path("pulseaudio-equalizer");
		DEFEQL = GLib.find_program_in_path("pulseaudio-equalizer-ladspa");
	}
	catch(e) { global.log("Error trying to find any equalizer"); }
	return inst;
}

function Config() {
	this._init();
}

Config.prototype = {
	_init: function() {
		this.load();
	},
	load: function() {
		try {
			GLib.spawn_command_line_sync(EXEC + " interface.getsettings");
			this._monitor = Gio.file_new_for_path(EQCONFIG).monitor(Gio.FileMonitorFlags.NONE, null);
			this._monitor.connect("changed", Lang.bind(this, function(self, file, otherFile, eventType) {
				if (eventType == Gio.FileMonitorEvent.CHANGES_DONE_HINT) {
					this._configChanged();
				}
			}));

			this._rawdata = Cinnamon.get_file_contents_utf8_sync(EQCONFIG).split('\n');
			this.presets = Cinnamon.get_file_contents_utf8_sync(EQPRESETS).split('\n')
						.filter(function(item) { return item.length > 0; });
		} catch (e) {
			global.logError(e);
		}
	},
	save: function() {
		try {
			this._monitor.cancel()
			let out = Gio.file_new_for_path(EQCONFIG).replace(null, false, Gio.FileCreateFlags.NONE, null);
			out.write_all(this._rawdata.join('\n'), null);
			out.close(null);

			GLib.spawn_command_line_sync(EXEC + " interface.applysettings");
			this.load();
			this.emit("changed");
		} catch (e) {
			global.logError(e);
		}
	},
	enabled: function() {
		return this._rawdata[5] == 1;
	},
	set_enabled: function(enabled) {
		this._rawdata[5] = enabled?1:0;
		this.save();
	},
	toggle: function() {
		this.set_enabled(!this.enabled());
	},
	preset: function() {
		return this._rawdata[4];
	},
	set_preset: function(preset) {
		let ppath = PRESETDIR1 + preset + ".preset";
		let file = Gio.file_new_for_path(ppath);
		let rawdata = null;
		if (DBG) global.log("EQ trying to load preset " + preset + "\nat " + ppath + "\nwhile file path is " + file.get_path());
		if (file.query_exists(null)) {
			rawdata = Cinnamon.get_file_contents_utf8_sync(file.get_path()).split('\n');
		} else {
			ppath = PRESETDIR2 + preset + ".preset";
			file = Gio.file_new_for_path(PRESETDIR2 + preset + ".preset");
			if (DBG) global.log("EQ trying to load preset " + preset + "\nat " + ppath + "\nwhile file path is " + file.get_path());
			if (file.query_exists(null)) {
				rawdata = Cinnamon.get_file_contents_utf8_sync(file.get_path()).split('\n');
			} else {
				ppath = PRESETDIR3 + preset + ".preset";
				file = Gio.file_new_for_path(PRESETDIR3 + preset + ".preset");
				if (DBG) global.log("EQ trying to load preset " + preset + "\nat " + ppath + "\nwhile file path is " + file.get_path());
				if (file.query_exists(null)) {
					rawdata = Cinnamon.get_file_contents_utf8_sync(file.get_path()).split('\n');
				} else {
					if (DBG) global.log("EQ preset error: file not found for " + preset);
					return;
				}
			}
		if (DBG) global.log("EQ preset found at " + file.get_path());
		}
		for (let i = 0; i < 5; ++i) {
			if (i == 3) {
			//_try to fix wrong preamp values, if it matters at all
				let v = parseFloat(rawdata[i]);
				if (v <= 0.0 || v > 1.0) {
					this._rawdata[i] = 1.0;
					continue;
				}
			}
			this._rawdata[i] = rawdata[i];
		}
		this._rawdata[9] = rawdata[5];
		this._rawdata = this._rawdata.slice(0, 10);
		for (let i = 10; i < (11+2*parseInt(rawdata[5])); ++i) {
			this._rawdata[i] = rawdata[i - 4];
		}
		if (DBG) global.log("EQ saving new preset setting");
		this.save();
	},
	_configChanged: function() {
		this._monitor.cancel();
		this.load();
		this.emit("changed");
	}
};
Signals.addSignalMethods(Config.prototype);

function EqualizerApplet(metadata, orientation, panel_height, instanceId) {
	this._init(metadata, orientation, panel_height, instanceId);
}

EqualizerApplet.prototype = {
	__proto__: Applet.IconApplet.prototype,
	_init: function(metadata, orientation, panel_height, instanceId) {
		Applet.IconApplet.prototype._init.call(this, orientation, panel_height, instanceId);
		this.id = instanceId; this.metadata = metadata; this.orientation = orientation;
		this.settings = new Settings.AppletSettings(this, this.metadata.uuid, this.id);
		this.settings.bind("notify", "notify", this.toggle_notify);
		this.settings.bind("startup", "startup", this.toggle_startup);
		this.settings.bind("prefeq", "prefeq", this.reload);
		this.settings.bind("debug", "debug", this.toggle_debug);
		this.inst = HasEq();
		if (this.inst.length == 0) {
			global.logError("pulseaudio-equalizer is not installed; disabling applet " + UUID);
			Util.spawnCommandLine("xdg-open " + HLPTXT);
			AppletManager._removeAppletFromPanel(UUID, this.id);
		} else if (this.inst.length == 1 && this.prefeq != this.inst[0]) {
			this.prefeq = this.inst[0];
		} else if (this.prefeq != "" && this.inst.includes(this.prefeq)) {
		} else {
			if (DEFEQL && this.inst.includes(DEFEQL)) this.prefeq = DEFEQL;
			else if (DEFEQ && this.inst.includes(DEFEQ)) this.prefeq = DEFEQ;
			else this.prefeq = this.inst[0];
//			this._buildDlg(true);
//			this.dialog.open();
		}
		this._init2();
	},
	_init2: function() {
		EXEC = this.prefeq;
		LOCAL = EXEC.includes("/local") ? "/local" : "";
		EXEGUI = EXEC + "-gtk";
		let exe = EXEC.split('/');
		PRESETDIR2 = "/usr" + LOCAL + "/share/" + exe[exe.length-1] + "-ladspa/presets/";
		PRESETDIR3 = "/usr" + LOCAL + "/share/" + exe[exe.length-1] + "/presets/";
		if (DBG) {
			let msg = "\n[" + UUID + "] debug report (part 2):" +
			"\nEXEC=" + EXEC + "\nLOCAL=" + LOCAL + "\nEXEGUI=" + EXEGUI +
			"\nPRESETDIR2=" + PRESETDIR2 + "\nPRESETDIR3=" + PRESETDIR3 +
			"\n[" + UUID + "] end report\n";
			global.log(msg);
		}
		if (EXEC == "")
			AppletManager._removeAppletFromPanel(UUID, this.id);
		try {
			this._notification = null; this.restore = null;
			this.state = EQDISAB;
			this.config = new Config();
			this.cvctrl = new Cvc.MixerControl({ name: "SndMixer" });
			this.cvctrl.connect("output-added", Lang.bind(this, this._addDevice));
			this.cvctrl.connect("active-output-update", Lang.bind(this, this._outputChanged, "update"));
			this.config.set_enabled(this.startup);

			Gtk.IconTheme.get_default().append_search_path(this.metadata.path);
			this.set_applet_icon_symbolic_name(this.config.enabled() ? EQON : EQOFF);

			this.menuManager = new PopupMenu.PopupMenuManager(this);
			this.menu = new Applet.AppletPopupMenu(this, this.orientation);
			this.menuManager.addMenu(this.menu);

			this._enabledSwitch = new PopupMenu.PopupSwitchMenuItem(PM_ENAB,
									this.config.enabled());
			this.menu.addMenuItem(this._enabledSwitch);
			this._enabledSwitch.connect("toggled", Lang.bind(this.config, this.config.toggle));

			this._presetsItem = new PopupMenu.PopupSubMenuMenuItem(PM_PRES);
			this.menu.addMenuItem(this._presetsItem);
			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem);

			this._settingsItem = new PopupMenu.PopupMenuItem(PM_SETT);
			this.menu.addMenuItem(this._settingsItem);
			this._settingsItem.connect("activate", function() {
			Util.spawnCommandLineAsync(EXEGUI, null, null);
			});
			this._applet_context_menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
		//_notification toggle
			this.notify_menu =  new PopupMenu.PopupIndicatorMenuItem(CM_NOTF);
			this.notify_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.notify); // value=1
			this.notify_menu.connect('activate', Lang.bind(this, function() {
				this.notify = !this.notify;
				this.notify_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.notify); // value=1
				this.settings.setValue("notify", this.notify);
			}));
			this._applet_context_menu.addMenuItem(this.notify_menu);
		//_startup-enabled toggle
			this.startup_menu =  new PopupMenu.PopupIndicatorMenuItem(CM_STRT);
			this.startup_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.startup); // value=1
			this.startup_menu.connect('activate', Lang.bind(this, function() {
				this.startup = !this.startup;
				this.startup_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.startup); // value=1
				this.settings.setValue("startup", this.startup);
			}));
			this._applet_context_menu.addMenuItem(this.startup_menu);
		//_switch equalizers
			this.sweq_menu =  new PopupMenu.PopupIconMenuItem(CM_SLCT,
				EQON, ITYPE);
			this.sweq_menu.connect('activate', Lang.bind(this, function() {
				this._buildDlg();
				this.dialog.open();
			}));
			this._applet_context_menu.addMenuItem(this.sweq_menu);
		//_context_menu_item_help
			if (GLib.file_test(HLPTXT, GLib.FileTest.EXISTS)) {
				this.help_menu =  new PopupMenu.PopupIconMenuItem(CM_HELP,
					"dialog-question", ITYPE);
				this.help_menu.connect('activate', Lang.bind(this, function() {
					try { Util.spawnCommandLine(`xdg-open ` + HLPTXT); }
					catch(e) { global.logError(UUID + ": 'xdg-open' cannot open the help file " + HLPTXT); }
				}));
				this._applet_context_menu.addMenuItem(this.help_menu);
			} else global.logError(UUID + ": missing help file " + HLPTXT);
		//_context_menu_item_reload_applet
			this.reload_menu = new PopupMenu.PopupIconMenuItem(CM_RELD, "reload", St.IconType.FULLCOLOR);
			this.reload_menu.connect('activate', Lang.bind(this, function() {
				Ext.reloadExtension(UUID, Ext.Type.APPLET);
			}));
			this._applet_context_menu.addMenuItem(this.reload_menu);
			this._applet_tooltip._tooltip.use_markup = true;
			this._applet_tooltip._tooltip.clutter_text.ellipsize = false;
			this.cvctrl.open();
			this.config.connect("changed", Lang.bind(this, this._configChanged));
			this._configChanged();
		} catch (e) {
			global.logError(e);
			AppletManager._removeAppletFromPanel(UUID, this.id);
		}
	},
	_setExec: function(i) {
		if (i == null) {
			if (EXEC != "") return;
			else AppletManager._removeAppletFromPanel(UUID, this.id);
		}
		this._log("EQ selected=" + this.inst[i] + ", i=" + i.toString());
		if (this.prefeq != this.inst[i]) {
			this.prefeq = this.inst[i];
			this._init2(); // Cinnamon has the nasty habit of keeping multiple instances
//			this.reload(); // running so better avoid reloading the applet
		}
	},
	_buildDlg: function(onstartup = false) {
		let found = ""; let btns = []; let i, idx, k, label;
		if (!onstartup) this.inst = HasEq();
		this.dialog = new Dialog.ModalDialog({cinnamonReactive: true});
		if (this.inst.length > 1) {
			for (i=0; i<this.inst.length; ++i) {
				found += "\n" + (i+1).toString() + ". " + this.inst[i];
				k = eval(`Clutter.KEY_${i+1}`); idx = i;
				btns.push({
					label: (i+1).toString(),
					focused: (i==0 ? true : false),
					action: Lang.bind(this, function(){
						this.dialog.destroy();
						this._setExec(idx);
					}),
					key: k
				});
			}
			btns.push({
				label: CANCEL,
				focused: false,
				action: Lang.bind(this, function(){
					this.dialog.destroy();
				}),
				key: Clutter.KEY_Escape
			});
			label = EQCHOICE + found;
			this.dialog.contentLayout.add(new St.Label({text: label}));
			this.dialog.setButtons(btns);
			this._log("EQ found:" + found);
		} else if (this.inst.length == 1) {
			btns.push({
				label: OK,
				focused: true,
				action: Lang.bind(this, function(){
					this.dialog.destroy();
				}),
				key: Clutter.KEY_Return
			});
			this.dialog.contentLayout.add(new St.Label({text: EQONE}));
			this.dialog.setButtons(btns);
			this._log("Only one EQ found: " + this.inst[0]);
		} else {
			btns.push({
				label: OK,
				focused: true,
				action: Lang.bind(this, function(){
					AppletManager._removeAppletFromPanel(UUID, this.id);
				}),
				key: Clutter.KEY_Return
			});
			this.dialog.contentLayout.add(new St.Label({text: EQNONE}));
			this.dialog.setButtons(btns);
			this._log("No EQ found in any of the known paths; applet disabled.");
		}
	},
	on_applet_clicked: function(event) {
		this.menu.toggle();
	},
	_configChanged: function() {
		let enabled = this.config.enabled();
		this._enabledSwitch.setToggleState(enabled);
		this._presetsItem.menu.removeAll();
		for (let i = 0; i < this.config.presets.length; ++i) {
			let preset = this.config.presets[i];
			let menuItem = new PopupMenu.PopupMenuItem(preset);
			if (preset === this.config.preset()) {
				menuItem.setShowDot(true);
			}
			menuItem.connect("activate", Lang.bind(this.config, function() {
				if (DBG) global.log("EQ preset changing to " + preset);
				this.set_preset(preset);
				if (DBG) global.log("EQ preset changed");
			}));
			this._presetsItem.menu.addMenuItem(menuItem);
		}
		this.set_applet_icon_symbolic_name(enabled ? EQON : EQOFF);
		this._setTooltip(enabled);
		if (this.restore) this._restore(enabled);
	},
	_setTooltip: function(enabled, out="") {
		let p = this.config.preset().replace(/&/g, '&amp;');
		let cp = p != "" ? " <b>" + p + "</b>" : " " + MSGNONE;
		this.state = enabled ? EQENAB : EQDISAB;
		let pre = enabled ? "\n" + MSGPRST + cp : "";
		let msg = out ? out + pre : pre; let tt = out ? this.state + "\n" + msg : this.state + msg;
		this._applet_tooltip._tooltip.get_clutter_text().set_markup(tt);
		this._applet_tooltip._tooltip.clutter_text.set_markup(tt);
		return msg;
	},
//_these won't work unless Cvc is open() ===>
	_addDevice: function(control, id) {
		let d = this.cvctrl["lookup_output_id"](id);
		if (d.description.match(/(Multiband\sEQ|Plugin|LADSPA)/g)) return;
		this._log("added output: " + d.description + ", " + d.origin);
	},
	_outputChanged: function(control, id, type="state") {
		let enabled = this.config.enabled();
		let out = this.cvctrl.get_default_sink();
		if (enabled === false || type=="state"|| !out) return;
		let d = this.cvctrl["lookup_output_id"](id);
		let desc = d.description; let orig = d.origin;
		if (!orig || desc.match(/(Multiband\sEQ|Plugin|LADSPA)/g)) {
			let msg = this._setTooltip(enabled, desc.match(/\s(on\s.*)/)[1].replace(/on/, MSGOUT));
			if (this.notify) this._notifyMessage(EQON, msg);
			this._log("found EQ, skipping");
			return;
		}
		this.restore = true;
		this.config.toggle();
		this._log(type + " output: " + desc + " < " + enabled.toString());
	},
	_restore: function(enabled) {
		this.restore = null;
		this.config.toggle();
		this._setTooltip(enabled);
		if (this.notify) this._notifyMessage(EQON);
	},
//_<===
	_ensureSource: function() {
		if (!this._source) {
			this._source = new MessageTray.Source();
			this._source.connect('destroy', () => this._source = null );
			if (Main.messageTray) Main.messageTray.add(this._source);
		}
	},
	_notifyMessage: function(iconName, text="") {
		if (this._notification)
			this._notification.destroy();
		this._ensureSource();
		let icon = new St.Icon({
			icon_name: iconName,
			icon_type: St.IconType.SYMBOLIC,
			icon_size: 32
		});
		try {
			this._notification = new MessageTray.Notification(this._source, this.state, text, {
				icon: icon, titleMarkup: true, bodyMarkup: true, bannerMarkup: true
			});
		} catch(e) {
			this._notification = new MessageTray.Notification(this._source, this.state, text, {
				icon: icon, titleMarkup: true, bodyMarkup: true
			});
		}
		this._notification.setUrgency(MessageTray.Urgency.NORMAL);
		this._notification.setTransient(true);
		this._notification.connect('destroy', Lang.bind(this, function() {
			this._notification = null;
		}));
		this._source.notify(this._notification);
	},
	toggle_notify: function() {
//		this.notify_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.notify);
	},
	toggle_startup: function() {
//		this.startup_menu.setOrnament(PopupMenu.OrnamentType.CHECK, this.startup);
	},
	toggle_debug: function() {
		DBG = this.debug;
	},
	reload: function() {
		Ext.reloadExtension(UUID, Ext.Type.APPLET);
	},
	on_applet_removed_from_panel: function() {
	  	try {
			if (this.config._monitor) {
				this.config._monitor.cancel();
			}
			Signals._disconnectAll(Config.prototype);
			this.settings.finalize();
		} catch(e) {global.logError("Equalizer2: " + e);}
	},
	_log: function(strg) {
		if (DBG) global.log(strg);
	}
};

function main(metadata, orientation, panel_height, instanceId) {
	return new EqualizerApplet(metadata, orientation, panel_height, instanceId);
}
